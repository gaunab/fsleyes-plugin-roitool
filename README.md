# fsleyes-plugin-roitool

A plugin which provides a easy way to find rois from Masks/Label-Images and get a fast information

## Usage

To pen the Plugin go to Settings->Ortho View->RoiTools. 

* all Overlays which are marked as ` mask image` or ` Label image` are listed in the Roi-Tools-Widget.
* You can drop down all Overlays to see the containing labels of each overlay
* DoubleClick on any label to jump to the center of mass of the label.

## Issues


## TODO
	
	* Mean/Std of each label in all overlays as table

## Changes
  * 0.0.6: Make Window visible with current fsleyes version
	* 0.0.5: Labelizing-Button adds labeled image to RoiList
